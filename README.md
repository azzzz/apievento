**Evento Api

The purpose of the application is to manage events.
The application, we can create new events, delete, update.
---

## GET

https://localhost:44359/event

---

## POST

https://localhost:44359/api/event

raw:
{
  "Name": "Nowy Event",
  "Description": "My event description",
  "StartDate": "2017-05-03",
  "EndDate": "2020-05-03",
  "Tickets": "1000",
  "Price": "50"
}

---

## PUT

Check Guid in method GET 

https://localhost:44359/event/56537b30-587f-408a-8e27-c6125929f5c2

raw:
{
  "Name": "Aktualizacja nazwy eventu",
  "Description": "My event description UPDATE"
}


---

## DELETE

https://localhost:44359/event/d651b7d1-23b9-4870-a33b-d05f509e92be

---
