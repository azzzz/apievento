﻿using Autofac;
using Evento.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Evento.Infrastructure.IoC.Modules
{
    public class EventServiceModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var assembly = typeof(EventServiceModule)
                .GetTypeInfo()
                .Assembly;

            builder.RegisterAssemblyTypes(assembly)
                   .Where(x => x.IsAssignableTo<IEventServiceMarker>())
                   .AsImplementedInterfaces()
                   .InstancePerLifetimeScope();
        }
    }
}
