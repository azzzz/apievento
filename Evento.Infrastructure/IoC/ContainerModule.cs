﻿
using Autofac;
using AutoMapper.Configuration;
using Evento.Infrastructure.IoC.Modules;
using Evento.Infrastructure.Mappers;
using Microsoft.Extensions.Configuration;

namespace Evento.Infrastructure.IoC
{
    public class ContainerModule : Autofac.Module
    {
        private readonly IConfigurationRoot _configuration;

        public ContainerModule(IConfigurationRoot configuration)
        {
            _configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterInstance(AutoMapperConfig.Initialize()).SingleInstance();
            builder.RegisterModule<RepositoryModule>();
            builder.RegisterModule<EventServiceModule>();
        }
    }
}
